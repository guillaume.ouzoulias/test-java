Bienvenue :)

Cette application permet de gérer une liste de tâches assignées à des responsables. 

La base de donnée est une base H2, sa définition est gérée par liquibase (cf. /src/main/resources/db/liquibase-changelog.xml)

L'objectif du test est le suivant : 

Définir les api suivantes : 

GET /taches --> renvoie la liste des tâches de la base de donnée
GET /taches/{id} --> renvoie les détails de la tâche donnée
POST /taches --> crée une tâche et l'assigne au responsable
PUT /taches --> mets à jour une tâche

Les api suivantes sont déjà déclarées

GET /responsables --> renvoie la liste des responsables
GET /responsables/{id} --> renvoie les détails d'un responsable
POST /responsables --> créer un responsable

L'api GET /responsables/{id} est à mettre à jour pour renvoyer la liste des tâches assignée à un responsable en plus des détails du responsable. 
Les détails d'un responsable devront aussi contenir le nombre total de tâches terminées assignées au responsable.

La liste des tâches doit contenir toutes les infos d'une tâche et les détails d'une tâche doivent contenir le nom et prénom du responsable.

À vous de déterminer la meilleure façon d'ajouter et d'implémenter ces apis. 

(ps : le swagger est disponible à l'url suivante : http://localhost:8080/swagger-ui/index.html)

Contactez emilien.nicolas@lyaprotect.com ou guillaume.ouzoulias@lyaprotect.com en cas de question sur la compréhension de l'énoncé
