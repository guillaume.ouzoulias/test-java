package com.lya.testjava.controller;

import com.lya.testjava.dto.request.ResponsableCreateRequest;
import com.lya.testjava.dto.response.ResponsableDetailResponse;
import com.lya.testjava.dto.response.ResponsableSimpleResponse;
import com.lya.testjava.service.ResponsableService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/responsables")
public class ResponsableController {

    private final ResponsableService responsableService;

    public ResponsableController(ResponsableService responsableService) {
        this.responsableService = responsableService;
    }

    @GetMapping
    public List<ResponsableSimpleResponse> listResponsables() {
        return responsableService.listResponsables().stream().map(ResponsableSimpleResponse::new).toList();
    }

    @GetMapping("/{id}")
    public ResponsableDetailResponse readResponsable(@PathVariable Long id) {
        return responsableService.getResponsable(id).map(ResponsableDetailResponse::new).orElse(null);
    }

    @PostMapping
    public ResponsableDetailResponse createResponsable(@RequestBody ResponsableCreateRequest responsableCreateRequest) {
        return new ResponsableDetailResponse(responsableService.saveResponsable(responsableCreateRequest.toEntity()));
    }

}
