package com.lya.testjava.dao;

import com.lya.testjava.model.Responsable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResponsableDAO extends JpaRepository<Responsable, Long> {
}
