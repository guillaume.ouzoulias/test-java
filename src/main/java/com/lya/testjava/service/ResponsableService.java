package com.lya.testjava.service;

import com.lya.testjava.dao.ResponsableDAO;
import com.lya.testjava.model.Responsable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ResponsableService {

    private final ResponsableDAO responsableDAO;

    public ResponsableService(ResponsableDAO responsableDAO) {
        this.responsableDAO = responsableDAO;
    }

    public List<Responsable> listResponsables(){
        return responsableDAO.findAll();
    }

    public Optional<Responsable> getResponsable(Long id){
        return responsableDAO.findById(id);
    }

    public Responsable saveResponsable(Responsable responsable){
        return responsableDAO.save(responsable);
    }
}
