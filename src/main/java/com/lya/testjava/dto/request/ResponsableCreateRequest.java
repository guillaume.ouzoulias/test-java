package com.lya.testjava.dto.request;

import com.lya.testjava.model.Responsable;

import java.util.Date;

public class ResponsableCreateRequest {
    private final String nom;
    private final String prenom;
    private final Date dateNaissance;

    public ResponsableCreateRequest(String nom, String prenom, Date dateNaissance) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
    }

    public Responsable toEntity(){
        Responsable responsable = new Responsable();
        responsable.setNom(this.getNom());
        responsable.setPrenom(this.getPrenom());
        responsable.setDateNaissance(this.getDateNaissance());
        return responsable;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

}
