package com.lya.testjava.dto.response;

import com.lya.testjava.model.Responsable;

import java.util.Date;

public class ResponsableDetailResponse {
    private final Long id;
    private final String nom;
    private final String prenom;
    private final Date dateNaissance;

    public ResponsableDetailResponse(Responsable responsable){
        this(responsable.getId(), responsable.getNom(), responsable.getPrenom(), responsable.getDateNaissance());
    }

    public ResponsableDetailResponse(Long id, String nom, String prenom, Date dateNaissance) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
    }

    public Long getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

}
