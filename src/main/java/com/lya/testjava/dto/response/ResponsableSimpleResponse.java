package com.lya.testjava.dto.response;

import com.lya.testjava.model.Responsable;

public class ResponsableSimpleResponse {

    private final Long id;
    private final String nom;
    private final String prenom;

    public ResponsableSimpleResponse(Responsable responsable){
        this(responsable.getId(), responsable.getNom(), responsable.getPrenom());
    }

    public ResponsableSimpleResponse(Long id, String nom, String prenom) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
    }

    public Long getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

}
